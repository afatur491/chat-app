import React, { useEffect, useState } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import SearchOutlined from "@material-ui/icons/SearchOutlined";
import AttachFile from "@material-ui/icons/AttachFile";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import "./../css/Chat.css";

function Chat() {
  const [seed, setSeed] = useState("");
  useEffect(() => {
    setSeed(Math.floor(Math.random() * 500));
  }, []);

  return (
    <div className="chat">
      <div className="chat_header">
        <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
        <div className="chat_headerInfo">
          <h3>Merry Si</h3>
          <p>Online</p>
        </div>
        <div className="chat_headerRight">
          <IconButton>
            <SearchOutlined />
          </IconButton>
          <IconButton>
            <AttachFile />
          </IconButton>
          <IconButton>
            <MoreVertIcon />
          </IconButton>
        </div>
      </div>
      <div className="chat_body">
        <p className={`chat_massage ${true && "chat_reciever"}`}>
          {/* <span className="chat_name">Merry Si</span> */}
          Hai Fatur
          <span className="chat_timestamp">16:55</span>
        </p>
        <p className={`chat_massage`}>
          {/* <span className="chat_name">Dika Purnama</span> */}
          Hallo
          <span className="chat_timestamp">16:57</span>
        </p>
      </div>
      <div className="chat_footer"></div>
    </div>
  );
}

export default Chat;
