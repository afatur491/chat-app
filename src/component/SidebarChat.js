import React, { useEffect, useState } from "react";
import { Avatar, IconButton } from "@material-ui/core";
import "./../css/SidebarChat.css";

function SidebarChat({ addNewChat }) {
  const [seed, setSeed] = useState("");
  useEffect(() => {
    setSeed(Math.floor(Math.random() * 500));
  }, []);

  const createChat = () => {
    const roomName = prompt("Masukkan Nama Anda");

    if (roomName) {
    }
  };

  return !addNewChat ? (
    <div className="sidebarChat">
      <Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
      <div className="sidebarChat_info">
        <h2>Merry Sin</h2>
        <p>Hallo</p>
      </div>
    </div>
  ) : (
    <div className="sidebarChat" onClick={createChat}>
      <h2>Obrolan Baru</h2>
    </div>
  );
}

export default SidebarChat;
